﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace soaga
{
    enum InputType
    {
        Up,
        Down,
        Left,
        Right,
        Jump,
        Action,
        WeakAttack,
        StrongAttack,
        Block,
        Channel,
    }
}
