﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace soaga
{
    class Enemy : Character
    {

        public Enemy(string filename, Size size, Size dimension, Dictionary<State, int[]> stateFrame, Size hitboxSize, Point hitboxOffset)
            : base(filename, size, dimension, stateFrame, hitboxSize, hitboxOffset)

        {
            
        }
    }
}
