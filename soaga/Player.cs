﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

using OpenTK;

namespace soaga
{
    class Player : Character
    {
        private Attack rightJab = new Attack(new Hitbox(new Size(8, 8),new Point(8, 8)), 100.0f);
        private Attack leftJab = new Attack(new Hitbox(new Size(8, 8), new Point(10, 8)), 100.0f);
         
        public Player()
            : base
            (
                "Data/Images/jacob.gif",
                new Size(2, 3),
                new Size(32, 32),
                new Dictionary<State, int[]>()
                {
                    { State.Idle, new int[2] { 0 , 0 } },
                    { State.Jump, new int[2] { 1 , 0 } },
                    { State.Run, new int[2] { 2 , 1 } },
                    { State.StrongAttack, new int[2] { 4 , 0 } },
                    { State.WeakAttack, new int[2] { 5 , 0 } }
                },
                new Size(22, 30),
                new Point(-9, 1)
            )
        {
            Initialise();
        }

        public void Initialise()
        {
            Initialise(new Vector2(128.0f, 128.0f), new Size(32, 32), 24, -0.21f, (int)State.Idle, Facing.Right);
            jumpPower = 6;
        }

        public void weakAttack()
        {
            if (!busy)
            {
                busy = true;
                changeState(State.WeakAttack);

                float direction;
                if (facing == Facing.Right)
                    direction = 1.0f;
                else
                    direction = -1.0f;
                leftJab.Initialise(new Vector2(1.0f * direction, 0.0f), 10);
                currentAttack = leftJab;
            }
        }
        public void strongAttack()
        {
            if (!busy)
            {
                busy = true;
                changeState(State.StrongAttack);
                
                float direction;
                if (facing == Facing.Right)
                    direction = 1.0f;
                else
                    direction = -1.0f;
                rightJab.Initialise(new Vector2(1.0f * direction, 0.0f), 15);
                currentAttack = rightJab;
            }
        }
    }
}
