﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK.Input;

namespace soaga
{
    abstract class Scene
    {
        abstract public void Initialise();
        abstract public void Update();
        abstract public void Render();
        abstract public void Dispose();
    }
}
