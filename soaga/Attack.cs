﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;

namespace soaga
{
    class Attack
    {
        public Hitbox hitbox { get; protected set; }
        public Vector2 direction { get; protected set; }

        public float duration { get; protected set; }
        public int damage { get; protected set; }


        public Attack(Hitbox hitbox, float duration)
        {
            this.hitbox = hitbox;
            this.duration = duration;
        }

        public void Initialise(Vector2 direction, int damage)
        {
            this.direction = direction;
            this.damage = damage;
        }
    }
}
