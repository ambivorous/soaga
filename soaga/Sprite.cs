﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace soaga
{
    class Sprite
    {
        public Texture texture { get; private set; }
        public Size size { get; private set; }
        public Size dimension { get; private set; }

        /*public StateFrame[] stateFrame { get; private set; }*/
        public Dictionary<State, int[]> stateFrame { get; private set; } // Array containing the state name, start point and number of frames

        public int frame { get; private set; }


        public Sprite(Texture texture, Size size, Size dimension, Dictionary<State, int[]> stateFrame)
        {
            this.texture = texture;
            this.size = size;
            this.dimension = dimension;
            this.stateFrame = stateFrame;
        }

        public void Initialise()
        {
            resetFrame();
        }

        public Point getOrigin(State state)
        // Fetches the pixel at which the desired sprite is on the spritesheet
        {
            Point origin = new Point((stateFrame[state][0] + frame) % size.Width * dimension.Width, (stateFrame[state][0] + frame) / (size.Width) * dimension.Height);

            return origin;
        }

        public void nextFrame(State state)
        // Cycles through the frames
        {
            if (stateFrame[state][1] == 0)
                return;

            if (stateFrame[state][1] > frame)
                frame++;
            else
                resetFrame();
        }

        public void resetFrame()
        {
            frame = 0;
        }
    }
}
