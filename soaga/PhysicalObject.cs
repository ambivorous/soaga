﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

using OpenTK;

namespace soaga
{
    class PhysicalObject : GameObject
    {
        public bool grounded;
        public Vector2 velocity;
        public Vector2 acceleration;
        public float mass { get; protected set; }


        public PhysicalObject(string filename, Size size, Size dimension, Dictionary<State, int[]> stateFrame, Size hitboxSize, Point hitboxOffset)
            : base(filename, size, dimension, stateFrame, hitboxSize, hitboxOffset)
        {
            
        }

        public void Initialise(Vector2 position, Size size, float mass, float ordering, State state, Facing facing)
        {
            Initialise(position, size, ordering, state, facing);
            this.mass = mass;

            grounded = false;
            velocity = new Vector2(0, 0);
            acceleration = new Vector2(0, 0);
        }

        public override void Update(int dt)
        {
            
        }

        public override void Render()
        {

        }

        public override void Collide(GameObject otherObject)
        {
            
        }

        public void checkGrounding(int groundHeight)
        {
            if ((int)position.Y == groundHeight)
                grounded = true;
            else
                grounded = false;
        }
    }
}
