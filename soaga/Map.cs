﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace soaga
{
    class Map
    {
        private Texture skyTexture;
        private Texture grassTexture;
        private Texture dirtTexture;

        public Size size { get; protected set; }
        public Size tileDimension { get; protected set; }

        public Tile[,] groundTileMatrix { get; protected set; }


        public Map()
        {
            size = new Size(64, 32);
            tileDimension = new Size(16, 16);

            skyTexture = new Texture();
            skyTexture.Load("Data/Images/sky.gif");
            grassTexture = new Texture();
            grassTexture.Load("Data/Images/grass.gif");
            dirtTexture = new Texture();
            dirtTexture.Load("Data/Images/dirt.gif");

            groundTileMatrix = new Tile[size.Width, size.Height];

            for (int i = 0; i < groundTileMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < groundTileMatrix.GetLength(1); j++)
                {
                    if (i >= 32)
                    {
                        if (j == 6)
                            groundTileMatrix[i, j] = new Tile(false, grassTexture, TileType.Grass);
                        else if (j < 6)
                            groundTileMatrix[i, j] = new Tile(false, dirtTexture, TileType.Dirt);
                        else
                            groundTileMatrix[i, j] = new Tile(true, skyTexture, TileType.Sky);
                    }
                    else if (i >= 16)
                    {
                        if (j == 4)
                            groundTileMatrix[i, j] = new Tile(false, grassTexture, TileType.Grass);
                        else if (j < 4)
                            groundTileMatrix[i, j] = new Tile(false, dirtTexture, TileType.Dirt);
                        else
                            groundTileMatrix[i, j] = new Tile(true, skyTexture, TileType.Sky);
                    }
                    else if (j == 5)
                        groundTileMatrix[i, j] = new Tile(false, grassTexture, TileType.Grass);
                    else if (j < 5)
                        groundTileMatrix[i, j] = new Tile(false, dirtTexture, TileType.Dirt);
                    else
                        groundTileMatrix[i, j] = new Tile(true, skyTexture, TileType.Sky);
                }
            }
        }

        public void Initialise()
        {

        }

        public void Render(Point offset)
        {
            for (int i = 0; i < groundTileMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < groundTileMatrix.GetLength(1); j++)
                {
                    GL.BindTexture(TextureTarget.Texture2D, groundTileMatrix[i, j].texture.id);
                    GL.Begin(PrimitiveType.Quads);

                    int origin = i * tileDimension.Width - offset.X;

                    GL.TexCoord2(0.0f, 0.0f);
                    GL.Vertex3(origin, j * tileDimension.Height + tileDimension.Height, -0.5f);
                    GL.TexCoord2(1.0f, 0.0f);
                    GL.Vertex3(origin + tileDimension.Width, j * tileDimension.Height + tileDimension.Height, -0.5f);
                    GL.TexCoord2(1.0f, 1.0f);
                    GL.Vertex3(origin + tileDimension.Width, j * tileDimension.Height, -0.5f);
                    GL.TexCoord2(0.0f, 1.0f);
                    GL.Vertex3(origin, j * tileDimension.Height, -0.5f);

                    GL.End();
                }
            }
        }

        public int getMapHeight(int x)
        {
            int height = 0;

            for (int j = 0; j < groundTileMatrix.GetLength(1); j++)
            {
                if (groundTileMatrix[x / tileDimension.Width, j].type == TileType.Grass)
                {
                    height = (j + 1) * tileDimension.Height;
                    break;
                }
            }

            return height;
        }

        public bool checkWallCollision(Point position, int speed)
        {
            // For this we assume getWallPosition is working and we're always in an open block

            if (position.X + speed < 0 || position.X + speed > size.Width * tileDimension.Width - 1)
                return true;

            if (!groundTileMatrix[(position.X + speed) / tileDimension.Width, position.Y / tileDimension.Height].passable)
                return true;

            return false;
        }
        public int getWallPosition(int position, int speed)
        {
            // For this we assume checkWallCollision is working and playerPos and wallPos are never equal

            if (position + speed < 0)
                return 0;
            else if (position + speed > size.Width * tileDimension.Width - 1)
                return size.Width * tileDimension.Width - 1;

            int wallPosition;

            int playerPos = position / tileDimension.Width;
            int impassableTilePos = (position + speed) / tileDimension.Width;
            
            if (playerPos > impassableTilePos)
                wallPosition = playerPos * tileDimension.Width;
            else
                wallPosition = impassableTilePos * tileDimension.Width - 1;

            return wallPosition;
        }

        public void Dispose()
        {
            skyTexture.Dispose();
        }
    }
}
