﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace soaga
{
    public enum State
    {
        Idle,
        Jump,
        Run,
        Alert,
        Hit,
        WeakAttack,
        StrongAttack,
    }
}
