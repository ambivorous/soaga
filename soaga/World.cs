﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace soaga
{
    class World
    {
        private float gravity;
        private int animationTimer;

        private Point offset;

        private Map map;
        private Player player;

        private List<GameObject> gameObjectList;
        private List<PhysicalObject> physicalObjectList;
        private List<Character> characterList;


        public World()
        {
            gameObjectList = new List<GameObject>();
            physicalObjectList = new List<PhysicalObject>();
            characterList = new List<Character>();

            map = new Map();

            player = new Player();
            gameObjectList.Add(player);
            physicalObjectList.Add(player);
            characterList.Add(player);
        }

        public void Initialise()
        {
            gravity = -0.25f;
            animationTimer = 0;

            offset = new Point(0, 0);

            Wolf wolf = new Wolf(new Vector2(512.0f, 128.0f), Facing.Left);
            gameObjectList.Insert(0, wolf);
            physicalObjectList.Add(wolf);
            characterList.Add(wolf);
        }

        public void Update(int dt)
        {
            // This makes the animation speed one frame per 100ms
            bool frameTick = false;
            animationTimer += dt;

            if (animationTimer > 100)
            {
                frameTick = true;
                animationTimer = 0;
            }

            // The input section? It's pretty self explanatory
            KeyboardState keyboard = OpenTK.Input.Keyboard.GetState();

            if (keyboard[Key.Space]) player.Jump();

            if (keyboard[Key.A] && !keyboard[Key.D])
            {
                if (player.position.X >= 0) player.moveLeft();
            }
            else if (!keyboard[Key.A] && keyboard[Key.D])
            {
                if (player.position.X < map.size.Width * map.tileDimension.Width) player.moveRight();
            }
            else
                player.acceleration.X = 0;

            if (keyboard[Key.J] && !keyboard[Key.K])
                player.weakAttack();
            if (!keyboard[Key.J] && keyboard[Key.K])
                player.strongAttack();

            Rectangle attackHitbox;
            Rectangle characterHitbox;
            Rectangle hitbox1;
            Rectangle hitbox2;

            for (int i = 0; i < gameObjectList.Count; i++)
            {
                gameObjectList[i].Update(dt);

                // Collision detection
                foreach (Character character in characterList)
                {
                    if (character.currentAttack == null)
                        continue;

                    foreach(Character otherCharacter in characterList)
                    {
                        if (character == otherCharacter)
                            continue;

                        attackHitbox = character.getcurrentAttackHitbox();
                        characterHitbox = otherCharacter.getHitbox();

                        if (attackHitbox.Left < characterHitbox.Right && attackHitbox.Right > characterHitbox.Left
                            && attackHitbox.Bottom > characterHitbox.Top && attackHitbox.Top < characterHitbox.Bottom)
                        {
                            otherCharacter.Hit();
                        }
                    }
                }

                for (int j = i + 1; j < gameObjectList.Count; j++)
                {
                    hitbox1 = gameObjectList[i].getHitbox();
                    hitbox2 = gameObjectList[j].getHitbox();

                    if (hitbox1.Left < hitbox2.Right && hitbox1.Right > hitbox2.Left && hitbox1.Bottom > hitbox2.Top && hitbox1.Top < hitbox2.Bottom)
                    {
                        gameObjectList[i].Collide(gameObjectList[j]);
                        gameObjectList[j].Collide(gameObjectList[i]);
                    }
                }

                // Animations
                if (frameTick && gameObjectList[i].sprite != null)
                    gameObjectList[i].sprite.nextFrame(gameObjectList[i].state);
            }

            // THE PHYSISCS ENGINE
            foreach (PhysicalObject physicalObject in physicalObjectList)
            {
                // Terminal velocity #hardcoded
                if ((physicalObject.velocity.X < -5.0f) ||
                    (physicalObject.velocity.X < 0 && physicalObject.acceleration.X < 0 && physicalObject.velocity.X + physicalObject.acceleration.X < -5.0f))
                {
                    physicalObject.velocity.X = -5.0f;
                    physicalObject.acceleration.X = 0;
                }
                else if ((physicalObject.velocity.X > 5.0f) ||
                    (physicalObject.velocity.X > 0 && physicalObject.acceleration.X > 0 && physicalObject.velocity.X + physicalObject.acceleration.X > 5.0f))
                {
                    physicalObject.velocity.X = 5.0f;
                    physicalObject.acceleration.X = 0;
                }
                else
                    physicalObject.velocity.X += physicalObject.acceleration.X;

                // More terminal velocity #hardcoded
                if (physicalObject.velocity.Y + physicalObject.acceleration.Y < -5.0f)
                    physicalObject.velocity.Y = -5.0f;
                else
                    physicalObject.velocity.Y += physicalObject.acceleration.Y;

                // Barbaric way of testing collisions with the ground
                if (physicalObject.velocity.X != 0 && map.checkWallCollision(physicalObject.getPositionInt(), (int)physicalObject.velocity.X))
                {
                    physicalObject.position.X = map.getWallPosition(physicalObject.getPositionInt().X, (int)physicalObject.velocity.X);
                    physicalObject.velocity.X = 0;
                }
                else
                    physicalObject.position.X += physicalObject.velocity.X;

                physicalObject.position.Y += physicalObject.velocity.Y;

                // Falling to the earth and not through it
                if (physicalObject.acceleration.Y == 0.0f)
                    physicalObject.acceleration.Y = gravity;
                 if (physicalObject.position.Y < map.getMapHeight(physicalObject.getPositionInt().X))
                {
                    physicalObject.position.Y = map.getMapHeight(physicalObject.getPositionInt().X);
                    physicalObject.velocity.Y = 0;
                    physicalObject.acceleration.Y = 0;
                }
                
                physicalObject.checkGrounding(map.getMapHeight(physicalObject.getPositionInt().X));

                // Dat ground drag
                if (physicalObject.grounded && physicalObject.velocity.X != 0 && physicalObject.acceleration.X == 0)
                {
                    if (physicalObject.velocity.X < 1.0f && physicalObject.velocity.X > -1.0f)
                        physicalObject.velocity.X = 0;
                    else
                        physicalObject.velocity.X = physicalObject.velocity.X * 0.95f;
                }
            }

            // Variable screen scrolling
            if (player.position.X - 160 < offset.X)
                offset.X = (player.getPositionInt().X - 160);
            else if (player.position.X + 160 > offset.X + 640)
                offset.X = (player.getPositionInt().X + 160) - 640;
        }

        public void Render(VertexBuffer vertexBuffer)
        {
            map.Render(offset);

            foreach (GameObject gameObject in gameObjectList)
            {
                // Blah blah openGL nonesense
                GL.BindTexture(TextureTarget.Texture2D, gameObject.sprite.texture.id);
                GL.Begin(PrimitiveType.Quads);

                Vector2 origin = gameObject.getSpriteOrigin();
                Vector2 dimension = gameObject.getSpriteDimension();

                int vertexOrigin = (gameObject.size.Width / 2);

                GL.TexCoord2(origin.X, origin.Y + dimension.Y);
                GL.Vertex3(-vertexOrigin + gameObject.position.X - offset.X, gameObject.position.Y, gameObject.ordering);
                GL.TexCoord2(origin.X + dimension.X, origin.Y + dimension.Y);
                GL.Vertex3(vertexOrigin + gameObject.position.X - offset.X, gameObject.position.Y, gameObject.ordering);
                GL.TexCoord2(origin.X + dimension.X, origin.Y);
                GL.Vertex3(vertexOrigin + gameObject.position.X - offset.X, gameObject.size.Height + gameObject.position.Y, gameObject.ordering);
                GL.TexCoord2(origin.X, origin.Y);
                GL.Vertex3(-vertexOrigin + gameObject.position.X - offset.X, gameObject.size.Height + gameObject.position.Y, gameObject.ordering);

                GL.End();
            }
        }

        public void Dispose()
        {
            map.Dispose();

            foreach (GameObject gameObject in gameObjectList)
                gameObject.Dispose();
            foreach (PhysicalObject physicalObject in physicalObjectList)
                physicalObject.Dispose();
            foreach (Character character in characterList)
                character.Dispose();
        }
    }
}
