﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

using OpenTK;

namespace soaga
{
    abstract class Character : PhysicalObject
    {
        protected bool busy;
        protected float actionTimer;

        public Attack currentAttack { get; protected set; }

        public int jumpPower { get; protected set; }


        public Character(string filename, Size size, Size dimension, Dictionary<State, int[]> stateFrame, Size hitboxSize, Point hitboxOffset)
            : base(filename, size, dimension, stateFrame, hitboxSize, hitboxOffset)
        {

        }

        public void Initialise(Vector2 position, Size size, float mass, float ordering, State state, Facing facing, int jumpPower)
        {
            Initialise(position, size, mass, ordering, state, facing);
            this.jumpPower = jumpPower;

            busy = false;
            actionTimer = 0.0f;
        }

        public override void Update(int dt)
        {
            if (busy)
                actionTimer += dt;

            if (currentAttack != null && actionTimer > currentAttack.duration)
            {
                actionTimer = 0.0f;
                busy = false;
                currentAttack = null;
            }

            if (!busy)
            {
                if (!grounded)
                    changeState(State.Jump);
                else if (velocity.X != 0.0f)
                    changeState(State.Run);
                else
                    changeState(State.Idle);
            }

            base.Update(dt);
        }

        public override void Collide(GameObject otherObject)
        {
            base.Collide(otherObject);
        }

        public void Hit()
        {
            changeState(State.Hit);
        }

        public Rectangle getcurrentAttackHitbox()
        {
            int offset = currentAttack.hitbox.offset.X;
            if (currentAttack.direction.X < 0)
            {
                offset += currentAttack.hitbox.size.Width;
                offset *= -1;
            }

            Point absoluteHitboxPosition = new Point(getPositionInt().X + offset, getPositionInt().Y + currentAttack.hitbox.offset.Y);
            Rectangle absoluteHitbox = new Rectangle(absoluteHitboxPosition, currentAttack.hitbox.size);

            return absoluteHitbox;
        }

        // Variable jump height crap
        public void Jump()
        {
            if (grounded)
            {
                velocity.Y += jumpPower;
                grounded = false;
            }
        }

        public void moveRight()
        {
            if (velocity.X < 1.5f && grounded && !busy)
                acceleration.X = 0.2f;
            else if (velocity.X < 1.5f && !grounded && !busy)
                acceleration.X = 0.1f;
            else
                acceleration.X = 0.0f;
            if (!busy)
                facing = Facing.Right; // Only change facing direction if the player purposefully actions it
        }
        public void moveLeft()
        {
            if (velocity.X > -1.5f && grounded && !busy)
                acceleration.X = -0.2f;
            else if (velocity.X > -1.5f && !grounded && !busy)
                acceleration.X = -0.1f;
            else
                acceleration.X = 0.0f;
            if (!busy)
                facing = Facing.Left; // Ditto
        }
    }
}
