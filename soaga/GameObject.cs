﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

using OpenTK;

namespace soaga
{
    abstract class GameObject
    {
        protected Texture texture;

        public Sprite sprite { get; protected set; }
        public Size size { get; protected set; }
        public Hitbox hitbox { get; protected set; }
        public Facing facing { get; protected set; }
        public Vector2 position;

        public State state { get; protected set; }
        public float ordering;


        public GameObject(string filename, Size size, Size dimension, Dictionary<State, int[]> stateFrame, Size hitboxSize, Point hitboxOffset)
        {
            texture = new Texture();
            texture.Load(filename);

            sprite = new Sprite(texture, size, dimension, stateFrame);
            hitbox = new Hitbox(hitboxSize, hitboxOffset);
        }

        public void Initialise(Vector2 position, Size size, float ordering, State state, Facing facing)
        {
            this.position = position;
            this.size = size;
            this.ordering = ordering;
            this.facing = facing;
            this.state = state;

            sprite.Initialise();
        }
        
        abstract public void Update(int dt);
        abstract public void Render();
        abstract public void Collide(GameObject otherObject);

        public Point getPositionInt()
        {
            return new Point((int)position.X, (int)position.Y);
        }

        public Rectangle getHitbox()
        {
            Point absoluteHitboxPosition = new Point(getPositionInt().X + hitbox.offset.X, getPositionInt().Y + hitbox.offset.Y);
            Rectangle absoluteHitbox = new Rectangle(absoluteHitboxPosition, hitbox.size);

            return absoluteHitbox;
        }

        public Vector2 getSpriteOrigin()
        {
            Point spriteOriginPixels = sprite.getOrigin(state);

            Vector2 spriteOrigin = new Vector2(spriteOriginPixels.X / (float)(sprite.size.Width * sprite.dimension.Width), spriteOriginPixels.Y / (float)(sprite.size.Height * sprite.dimension.Height));

            if (facing == Facing.Left)
                spriteOrigin.X -= getSpriteDimension().X;

            return spriteOrigin;
        }
        public Vector2 getSpriteDimension()
        {
            Vector2 spriteDimension = new Vector2(1.0f / sprite.size.Width, 1.0f / sprite.size.Height);

            if (facing == Facing.Left)
                spriteDimension.X *= -1;

            return spriteDimension;
        }

        public void changeState(State state)
        // Automatically resets the frame to the first frame when you change states
        {
            if (this.state != state)
            {
                sprite.resetFrame();
                this.state = state;
            }
        }

        public void Dispose()
        {
            if (texture != null)
                texture.Dispose();
        }
    }
}
