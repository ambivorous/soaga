﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace soaga
{
    class Tile
    {
        public bool passable { get; protected set; }

        public Texture texture { get; protected set; }
        public TileType type { get; protected set; }

        public Tile(bool passable, Texture texture, TileType type)
        {
            this.passable = passable;
            this.texture = texture;
            this.type = type;
        }
    }
}
