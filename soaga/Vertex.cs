﻿using System.Runtime.InteropServices;

using OpenTK;

namespace soaga
{
    struct Vertex
    {
        public Vector2 textureCoord;
        public Vector4 vertexCoord;

        public static readonly int Stride = Marshal.SizeOf(default(Vertex));
    }
}
