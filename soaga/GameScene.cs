﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace soaga
{
    class GameScene : Scene
    {
        private KeyboardState keyboard;
        private Stopwatch stopwatch;
        private World world;
        private VertexBuffer worldVb;

        private Input up;
        private Input down;
        private Input left;
        private Input right;
        private Input jump;
        private Input action;
        private Input weakAttack;
        private Input strongAttack;
        private Input block;
        private Input channel;

        public GameScene()
        {
            world = new World();
            worldVb = new VertexBuffer();
            stopwatch = new Stopwatch();

            int maxInputBufferTime = 2000;
            up = new Input(InputType.Up, maxInputBufferTime);
            down = new Input(InputType.Down, maxInputBufferTime);
            left = new Input(InputType.Left, maxInputBufferTime);
            right = new Input(InputType.Right, maxInputBufferTime);
            jump = new Input(InputType.Jump, maxInputBufferTime);
            action = new Input(InputType.Action, maxInputBufferTime);
            weakAttack = new Input(InputType.WeakAttack, maxInputBufferTime);
            strongAttack = new Input(InputType.StrongAttack, maxInputBufferTime);
            block = new Input(InputType.Block, maxInputBufferTime);
            channel = new Input(InputType.Channel, maxInputBufferTime);

            Initialise();
        }

        public override void Initialise()
        {
            stopwatch.Start();
            world.Initialise();
        }

        public override void Update()
        {
            int dt = (int)stopwatch.ElapsedMilliseconds;
            stopwatch.Restart();

            // The input section? It's pretty self explanatory
            keyboard = Keyboard.GetState();

            if (keyboard[Key.W]) up.IsPressed();
            else up.IsReleased();
            if (keyboard[Key.X]) down.IsPressed();
            else down.IsReleased();
            if (keyboard[Key.A]) left.IsPressed();
            else left.IsReleased();
            if (keyboard[Key.D]) right.IsPressed();
            else right.IsReleased();
            if (keyboard[Key.Space]) jump.IsPressed();
            else jump.IsReleased();
            if (keyboard[Key.L]) action.IsPressed();
            else action.IsReleased();
            if (keyboard[Key.J]) weakAttack.IsPressed();
            else weakAttack.IsReleased();
            if (keyboard[Key.K]) strongAttack.IsPressed();
            else strongAttack.IsReleased();
            if (keyboard[Key.ShiftLeft]) block.IsPressed();
            else block.IsReleased();
            if (keyboard[Key.ControlLeft]) channel.IsPressed();
            else channel.IsReleased();

            world.Update(dt);
        }

        public override void Render()
        {
            world.Render(worldVb);

            /*GL.EnableClientState(ArrayCap.VertexArray);
            GL.DrawArrays(BeginMode.Quads, )*/

            /*GL.DrawElements(BeginMode.Quads, 1, DrawElementsType.UnsignedInt, worldWbo);*/
        }

        public override void Dispose()
        {
            stopwatch.Stop();
            world.Dispose();

            worldVb.Dispose();
        }
    }
}
