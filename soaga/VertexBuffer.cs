﻿using System;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace soaga
{
    class VertexBuffer
    {
        public int id { get; private set; }
        public int dataLength { get; private set; }

        public VertexBuffer()
        {
            id = GL.GenBuffer();
            if (id == 0)
                throw new Exception("Could not create VBO.");
        }

        public void Load(Vertex[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            dataLength = data.Length;

            GL.BindBuffer(BufferTarget.ArrayBuffer, id);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(dataLength * Vertex.Stride), data, BufferUsageHint.StaticDraw);

            /*GL.BindBuffer(BufferTarget.ArrayBuffer, 0);*/
        }
        
        public void Render()
        {
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);

            GL.BindBuffer(BufferTarget.ArrayBuffer, id);
            GL.VertexPointer(4, VertexPointerType.Float, Vertex.Stride, new IntPtr(0));
            GL.TexCoordPointer(2, TexCoordPointerType.Float, Vertex.Stride, new IntPtr(Vector4.SizeInBytes));
            GL.DrawArrays(PrimitiveType.Quads, 0, dataLength);

            /*GL.BindBuffer(BufferTarget.ArrayBuffer, 0);*/
        }

        public void Dispose()
        {
            GL.DeleteBuffer(id);
        }
    }
}
