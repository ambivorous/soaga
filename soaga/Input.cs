﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace soaga
{
    class Input
    {
        public InputType type { get; protected set; }
        public int maxTime;

        public bool pressed;
        public bool transition;

        public bool keyPressed;

        public List<int> timeline;

        public Input(InputType type, int maxTime)
        {
            timeline = new List<int>();

            this.maxTime = maxTime;
            this.type = type;

            keyPressed = false;
            pressed = false;
        }

        public void IsPressed()
        {
            if (!keyPressed)
            {
                keyPressed = true;
                timeline.Add(maxTime);
            }
        }
        public void IsReleased()
        {
            if (keyPressed)
            {
                keyPressed = false;
                timeline.Add(maxTime);
            }
        }

        public bool getState()
        {
            return getState(0);
        }
        public bool getState(int atTime)
        {
            int count = 0;

            for (int i = 0; i < timeline.Count(); i++)
            {
                if (timeline[i] > atTime)
                    count++;
            }

            if (count % 2 == 0)
                return keyPressed;
            else
                return !keyPressed;
        }

        public void Update(int dt)
        {
            for (int i = 0; i < timeline.Count(); i++)
            {
                timeline[i] -= dt;

                if (timeline[i] < 0)
                    timeline.RemoveAt(i);
            }
        }
    }
}
