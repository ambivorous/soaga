﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace soaga
{
    class Hitbox
    {
        public Size size { get; protected set; }
        public Point offset { get; protected set; }

        public Hitbox (Size size, Point offset)
        {
            this.size = size;
            this.offset = offset;
        }
    }
}
