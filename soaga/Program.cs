﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace soaga
{
    public class Program : GameWindow
    {
        private Scene scene { get; set; }

        public Program() : base(640, 360, GraphicsMode.Default, "SoaGA v16.0.1")
        {
            KeyDown += new EventHandler<KeyboardKeyEventArgs>(Keyboard_KeyDown);
        }

        void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Exit();
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            VSync = VSyncMode.On;

            // Set opengl view options
            GL.ClearColor(Color.CornflowerBlue);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);
            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.TexEnv(TextureEnvTarget.TextureEnv, TextureEnvParameter.TextureEnvMode, (float)TextureEnvMode.Replace);

            scene = new GameScene();
        }

        protected override void OnResize(EventArgs e)
        {
            // Standard OpenTK code for window resize
            base.OnResize(e);

            GL.Viewport(0, 0, Width, Height);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            scene.Update();
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            // Camera
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0.0, (float)Width, 0.0, (float)Height, 0.0, 4.0);

            scene.Render();

            SwapBuffers();
        }

        [STAThread]
        public static void Main()
        {
            using (Program p = new Program())
            {
                p.Run(120f);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (scene != null)
                    scene.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
