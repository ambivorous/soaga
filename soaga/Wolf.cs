﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

using OpenTK;

namespace soaga
{
    class Wolf : Enemy
    {

        public Wolf(Vector2 position, Facing facing)
            : base
            (
                "Data/Images/wolf.gif",
                new Size(2, 2),
                new Size(32, 32),
                new Dictionary<State, int[]>()
                {
                    { State.Idle, new int[2] { 0 , 0 } },
                    { State.Jump, new int[2] { 1 , 0 } },
                    { State.Alert, new int[2] { 2 , 0 } },
                    { State.Hit, new int[2] { 3, 0 } }
                },
                new Size(30, 22),
                new Point(-15, 2)
            )
        {
            Initialise(position, facing);
    }
    
    public void Initialise(Vector2 position, Facing facing)
    {
        Initialise(position, new Size(32, 32), 24, -0.31f, (int)State.Idle, facing, 6);
    }
}
}
